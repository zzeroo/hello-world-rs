# Rust examples

[![pipeline status](https://gitlab.com/zzeroo/hello-world-rs/badges/master/pipeline.svg)](https://gitlab.com/zzeroo/hello-world-rs/-/commits/master)

This repository contains a very basic 'hello-world' example.

This project should build on all Rust versions and in all OS environments.

## gtk3 branch
[![pipeline status](https://gitlab.com/zzeroo/hello-world-rs/badges/gtk3/pipeline.svg)](https://gitlab.com/zzeroo/hello-world-rs/-/commits/gtk3)

There is a `gtk3` branch with a minimal gtk3 example.

Try it via:

```bash
git checkout gtk3
cargo build
cargo test
cargo run
```
